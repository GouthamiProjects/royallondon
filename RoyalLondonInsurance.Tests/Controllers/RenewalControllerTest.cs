﻿using System.Net;
using NUnit.Framework;
using Moq;
using RoyalLondonInsurance.Controllers;
using RoyalLondonInsurance.Service;
using System.Web.Http;
using System.Net.Http;
using RoyalLondonInsurance.Models;

namespace RoyalLondonInsurance.Tests.Controllers
{
    [TestFixture]
    public class RenewalControllerTest
    {
        Mock<IGenerator> _generator;
        
        [SetUp]
        public void SetUp()
        {
            _generator = new Mock<IGenerator>();

        }

        [Test]
        public void ShouldGenerateRenewalLetter()
        {
            Result result = new Result();
            result.IsGenerated = true;
            result.Message = "Document Generated";
            _generator.Setup(x => x.RenewalGenerator()).Returns(result);
            RenewalController controller = new RenewalController(_generator.Object);

            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();


            var resultGenerated = controller.GenerateRenewal();
            Assert.That(resultGenerated.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        }

        [Test]
        public void ShouldNotGenerateRenewalLetter()
        {
            Result result = new Result();
            result.IsGenerated = false;
            result.Message = "Document Not Generated";
            _generator.Setup(x => x.RenewalGenerator()).Returns(result);
            RenewalController controller = new RenewalController(_generator.Object);

            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();

            var resultGenerated = controller.GenerateRenewal();
            Assert.That(resultGenerated.StatusCode, Is.EqualTo(HttpStatusCode.InternalServerError));

        }
    }
}
