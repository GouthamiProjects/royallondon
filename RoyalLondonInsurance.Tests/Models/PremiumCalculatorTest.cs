﻿using NUnit.Framework;
using RoyalLondonInsurance.Models;
using Moq;
using RoyalLondonInsurance.Service;

namespace RoyalLondonInsurance.Tests.Models
{
    [TestFixture]
    public class PremiumCalculatorTest
    {
        private const decimal PayoutAmount = 1000M;
        private const decimal AnnualPremium = 50M;
        private const decimal CreditCharge = 2.5M;
        private const decimal TotalDebitPremium = 52.5M;
        private const decimal AverageMonthlyPremium = 4.375M;
        private Mock<IProduct> _mockProduct;
        private Mock<IUser> _mockUser;

        [SetUp]
        public void SetUp()
        {
            _mockProduct = new Mock<IProduct>();
            _mockProduct.Setup(s => s.PayoutAmount).Returns(PayoutAmount);
            _mockProduct.Setup(s => s.AnnualPremium).Returns(AnnualPremium);
            _mockProduct.Setup(s => s.CreditCharge).Returns(CreditCharge);
            _mockProduct.Setup(s => s.TotalDebitPremium).Returns(TotalDebitPremium);
            _mockProduct.Setup(s => s.AverageMonthlyPremium).Returns(AverageMonthlyPremium);

            _mockUser = new Mock<IUser>();
            _mockUser.Setup(s => s.Product).Returns(_mockProduct.Object);
        }

        /// <summary>
        /// Calculate the Premium
        /// </summary>
        [Test]
        public void ShouldCalculatePremium()
        {
            
            var premiumCalculator = new PremiumCalculator();
            premiumCalculator.CalculatePremium(_mockUser.Object);

            _mockUser.VerifySet(x => x.Product.CreditCharge = 2.5M, Times.Once);
            _mockUser.VerifySet(x=>x.Product.TotalDebitPremium = 52.5M, Times.Once);
            _mockUser.VerifySet(x => x.Product.AverageMonthlyPremium = 4.375M, Times.Once);
            _mockUser.VerifySet(x => x.Product.InitialMonthlyPaymentAmount = 4.43M, Times.Once);
            _mockUser.VerifySet(x => x.Product.OtherMonthlyPaymentAmount = 4.37M, Times.Once);
        }
        
    }
}