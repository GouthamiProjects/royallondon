﻿using System;
using System.IO;
using System.Web.Configuration;
using Moq;
using NUnit.Framework;
using RoyalLondonInsurance.Models;
using RoyalLondonInsurance.Service;
using Assert = NUnit.Framework.Assert;

namespace RoyalLondonInsurance.Tests.Models
{
    [TestFixture]
    public class DocumentGeneratorTest
    {
        private readonly string _folderDirectory = WebConfigurationManager.AppSettings["FolderLocation"];
        private const decimal PayoutAmount = 1000M;
        private const decimal AnnualPremium = 50M;
        private const decimal CreditCharge = 2.5M;
        private const decimal TotalDebitPremium = 52.5M;
        private const decimal AverageMonthlyPremium = 4.375M;
        private Mock<IProduct> _mockProduct;
        private Mock<IUser> _mockUser;
        private const int Id = 00001;
        private const string FirstName = "Sachin";
        private const string SurName = "Tendulkar";
        private const string Title = "Mr";

        [SetUp]
        public void SetUp()
        {
            _mockProduct = new Mock<IProduct>();
            _mockProduct.Setup(s => s.PayoutAmount).Returns(PayoutAmount);
            _mockProduct.Setup(s => s.AnnualPremium).Returns(AnnualPremium);
            _mockProduct.Setup(s => s.CreditCharge).Returns(CreditCharge);
            _mockProduct.Setup(s => s.TotalDebitPremium).Returns(TotalDebitPremium);
            _mockProduct.Setup(s => s.AverageMonthlyPremium).Returns(AverageMonthlyPremium);

            _mockUser = new Mock<IUser>();
            _mockUser.Setup(s => s.Id).Returns(Id);
            _mockUser.Setup(s => s.Title).Returns(Title);
            _mockUser.Setup(s => s.FirstName).Returns(FirstName);
            _mockUser.Setup(s => s.SurName).Returns(SurName);
            _mockUser.Setup(s => s.Product).Returns(_mockProduct.Object);
        }

        /// <summary>
        /// Check whether Document is generated in the exact place
        /// </summary>
        [Test]
        public void ShouldGenerateLetter()
        {
            try
            {
                WebConfigurationManager.AppSettings["FolderLocation"] = "Test";
                var documentGenerator = new DocumentGenerator();
                documentGenerator.GenerateRenewalLetter(_mockUser.Object);

                var fileName = Id + "_" + FirstName + "_" + SurName;
                var filePath = _folderDirectory + fileName + ".docx";
                var actualResult = File.Exists(filePath);

                Assert.AreEqual(true, actualResult);
            }
            catch (Exception e)
            {
                Assert.Fail();
            }
        }

    }
}