﻿using System.Web.Configuration;
using NUnit.Framework;
using RoyalLondonInsurance.Models;
using RoyalLondonInsurance.Service;
using Assert = NUnit.Framework.Assert;

namespace RoyalLondonInsurance.Tests.Models
{
    [TestFixture]
    public class FileManagerTest
    {
        private readonly string _folderDirectory = WebConfigurationManager.AppSettings["FolderLocation"];
        private const int Id = 10;
        private const string FirstName = "Meera";
        private const string SurName = "Bhai";

        /// <summary>
        /// Validate File
        /// </summary>
        [Test]
        public void ShouldValidateFile()
        {
            var fileManager = new FileManager();

            var actualResult = fileManager.ValidateFile(Id, FirstName, SurName);
            Assert.AreEqual(false, actualResult);
        }

        /// <summary>
        /// Get File Path
        /// </summary>
        [Test]
        public void ShouldGetFilePath()
        {
            var fileManager = new FileManager();
            var expectedResult = _folderDirectory;

            var actualResult = fileManager.GetFilePath();
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}