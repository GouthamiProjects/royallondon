﻿using RoyalLondonInsurance.Models;

namespace RoyalLondonInsurance.Factory
{
    public sealed class ModelFactory : IModelFactory
    {
        #region Methods
        /// <summary>
        /// Create the Instance of Product
        /// </summary>
        /// <param name="productName"></param>
        /// <param name="payoutAmount"></param>
        /// <param name="annualPremium"></param>
        /// <returns></returns>
        public IProduct CreateProduct(string productName, decimal payoutAmount, decimal annualPremium)
        {
            return new Product(productName, payoutAmount, annualPremium);
        }

        /// <summary>
        /// Create the Instance of User
        /// </summary>
        /// <param name="id"></param>
        /// <param name="title"></param>
        /// <param name="firstName"></param>
        /// <param name="surName"></param>
        /// <param name="product"></param>
        /// <returns></returns>
        public IUser CreateUser(int id, string title, string firstName, string surName, IProduct product)
        {
            return new User(id, title, firstName, surName, product);
        }
        #endregion
    }
}