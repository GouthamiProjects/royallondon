﻿using RoyalLondonInsurance.Models;

namespace RoyalLondonInsurance.Factory
{
    public interface IModelFactory
    {
        IProduct CreateProduct(string productName, decimal payoutAmount, decimal annualPremium );
        IUser CreateUser(int id, string title, string firstName, string surName, IProduct product);
    }
}