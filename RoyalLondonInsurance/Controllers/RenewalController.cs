﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RoyalLondonInsurance.Service;

namespace RoyalLondonInsurance.Controllers
{
    public class RenewalController : ApiController
    {
        #region Fields
        private readonly IGenerator _generator;
        #endregion

        #region Construtor
        public RenewalController(IGenerator generator)
        {
            _generator = generator;
        }
        #endregion


        #region Methods
        //http://localhost:59950/api/Renewal
        [HttpPost]
        public HttpResponseMessage GenerateRenewal()
        {
            try
            {
                var result = _generator.RenewalGenerator();
                return result.IsGenerated ? Request.CreateResponse(HttpStatusCode.OK, result) : Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
