﻿namespace RoyalLondonInsurance.Models
{
    public interface IUser
    {
        int Id { get; }

        string Title { get; }

        string FirstName { get; }

        string SurName { get; }

        IProduct Product { get; }

        bool IsValid { get; }

        string ValidationMessage { get; }
    }
}