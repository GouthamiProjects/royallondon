﻿namespace RoyalLondonInsurance.Models
{
    public enum Title
    {
       Miss = 0,
       Mr,
       Mrs
    }
}