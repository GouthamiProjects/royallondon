﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoyalLondonInsurance.Models
{
    public class Result
    {
        public bool IsGenerated { get; set; }

        public string Message { get; set; }
    }
}