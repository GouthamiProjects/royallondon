using System.Web.Http;
using RoyalLondonInsurance.Factory;
using RoyalLondonInsurance.Models;
using RoyalLondonInsurance.Service;
using Unity;
using Unity.WebApi;

namespace RoyalLondonInsurance
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IFileManager, FileManager>();
            container.RegisterType<IModelFactory, ModelFactory>();
            container.RegisterType<IDocumentGenerator, DocumentGenerator>();
            container.RegisterType<IPremiumCalculator, PremiumCalculator>();
            container.RegisterType<IGenerator, Generator>();
            



            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}