﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using Microsoft.Office.Interop.Excel;
using RoyalLondonInsurance.Models;

namespace RoyalLondonInsurance.Service
{
    public interface IExcelReader
    {
        Range ReadExcelData();

    }
}