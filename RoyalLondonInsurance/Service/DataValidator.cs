﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Microsoft.Office.Interop.Excel;
using RoyalLondonInsurance.Factory;
using RoyalLondonInsurance.Models;

namespace RoyalLondonInsurance.Service
{
    public class DataValidator : IDataValidator
    {
        #region Fields
        readonly IFileManager _fileManager;
        readonly IModelFactory _modelFactory;
        #endregion

        #region Contructors
        public DataValidator(IFileManager fileManager, IModelFactory modelFactory)
        {
            _fileManager = fileManager;
            _modelFactory = modelFactory;
        }
        #endregion
        
        #region Methods
        /// <summary>
        /// Validate the Excel Data and create the User Model
        /// </summary>
        /// <param name="excelRange"></param>
        /// <returns></returns>
        public List<IUser> ValidateExcelDataAndCreateModel(Range excelRange)
        {
            var userList = new List<IUser>();

            for (int row = 2; row <= excelRange.Rows.Count; row++)
            {
                var builder = new System.Text.StringBuilder();

                User user;
                var id = ((Range)excelRange.Cells[row, 1]).Text;
                var title = ((Range)excelRange.Cells[row, 2]).Text;
                var firstName = ((Range)excelRange.Cells[row, 3]).Text;
                var surName = ((Range)excelRange.Cells[row, 4]).Text;
                var productName = ((Range)excelRange.Cells[row, 5]).Text;
                var payoutAmount = ((Range)excelRange.Cells[row, 6]).Text;
                var annualPremium = ((Range)excelRange.Cells[row, 7]).Text;

                int outId;
                var isIdValid = int.TryParse(Convert.ToString(id), out outId);

                //Validate Id
                if (!isIdValid)
                    builder.Append("Id,");

                //Validate Title
                Title outTitle;
                var isTitleValid = Enum.TryParse(Convert.ToString(title), false, out outTitle);
                if (!isTitleValid)
                    builder.Append(" Title,");

                //Validate PayOutAmount
                decimal outPayoutamount;
                var isPayoutAmountValid = decimal.TryParse(Convert.ToString(payoutAmount, CultureInfo.InvariantCulture), out outPayoutamount);

                //Validte Premium
                decimal outAnnualPremium;
                var isAnnualPremiumValid = decimal.TryParse(Convert.ToString(annualPremium), out outAnnualPremium);

                if (!isPayoutAmountValid)
                    builder.Append(" PayOut Amount,");

                if (!isAnnualPremiumValid)
                    builder.Append(" Annual Premium,");

                if (!isTitleValid || !isPayoutAmountValid || !isAnnualPremiumValid)
                    builder.Append("is not valid for the Customer ID  " + id + "." + Environment.NewLine);

                //Validate File Exist or not
                bool userExist = _fileManager.ValidateFile(Convert.ToInt32(id), Convert.ToString(firstName), Convert.ToString(surName));
                if (userExist)
                {
                    builder.Append(" File Already Exist for the Customer ID " + id + ".");
                }

                //If no Error, Create the User Model
                if (builder.ToString().Equals(""))
                {
                    var product = _modelFactory.CreateProduct(productName, Convert.ToDecimal(payoutAmount), Convert.ToDecimal(annualPremium));
                    user = new User { IsValid = true, Id = Convert.ToInt32(id), Title = title, FirstName = firstName, SurName = surName, Product = product };
                }
                else
                {
                    user = new User { IsValid = false, ValidationMessage = builder.ToString() };
                }
                userList.Add(user);
            }
            return userList;
        }
        #endregion

    }
}