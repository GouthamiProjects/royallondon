﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Microsoft.Office.Interop.Excel;
using RoyalLondonInsurance.Models;

namespace RoyalLondonInsurance.Service
{
    public interface IDataValidator
    {
        List<IUser> ValidateExcelDataAndCreateModel(Range excelRange);

    }
}