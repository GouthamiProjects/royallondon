﻿using RoyalLondonInsurance.Models;

namespace RoyalLondonInsurance.Service
{
    public interface IDocumentGenerator
    {
        void GenerateRenewalLetter(IUser userDetails);
    }
}