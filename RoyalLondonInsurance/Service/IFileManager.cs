﻿namespace RoyalLondonInsurance.Service
{
    public interface IFileManager
    {
        bool ValidateFile(int id, string firstName, string surName);

        string GetFilePath();
    }
}