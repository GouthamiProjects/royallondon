﻿using RoyalLondonInsurance.Models;

namespace RoyalLondonInsurance.Service
{
    public interface IGenerator
    {
        Result RenewalGenerator();
    }
}