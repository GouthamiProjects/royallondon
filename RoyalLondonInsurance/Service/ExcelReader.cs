﻿using System.Web.Configuration;
using Microsoft.Office.Interop.Excel;

namespace RoyalLondonInsurance.Service
{
    public class ExcelReader : IExcelReader
    {
        private readonly string _folderDirectory = WebConfigurationManager.AppSettings["FolderLocation"];
      
        /// <summary>
        /// Read Excel Data
        /// </summary>
        /// <returns></returns>
        public Range ReadExcelData()
        {
            var path = _folderDirectory + "Customer.csv";
            var application = new Application();
            var workbook = application.Workbooks.Open(path);
            var workSheet = (Worksheet)workbook.ActiveSheet;
            var excelRange = workSheet.UsedRange;
            return excelRange;
        }
    }
}