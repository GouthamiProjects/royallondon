﻿using RoyalLondonInsurance.Models;

namespace RoyalLondonInsurance.Service
{
    public interface IPremiumCalculator
    {
        void CalculatePremium(IUser userDetails);        
    }
}