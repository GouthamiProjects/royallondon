﻿using RoyalLondonInsurance.Models;

namespace RoyalLondonInsurance.Service
{
    public class Generator : IGenerator
    {
        #region Fields
        readonly IDocumentGenerator _documentGeneratorInstance;
        readonly IPremiumCalculator _premiumCalculatorInstance;
        readonly DataValidator _dataValidation;
        #endregion

        #region Contructors
        public Generator(IDocumentGenerator documentGeneratorInstance, IPremiumCalculator premiumCalculatorInstance, DataValidator dataValidation)
        {
            _documentGeneratorInstance = documentGeneratorInstance;
            _premiumCalculatorInstance = premiumCalculatorInstance;
            _dataValidation = dataValidation;
        }
        #endregion
        
        #region Methods
        /// <summary>
        /// Generate Renewal Letter
        /// </summary>
        /// <returns></returns>
        public Result RenewalGenerator()
        {
            Result result = new Result();
            var stringBuilder = new System.Text.StringBuilder();

            //Read Excel
            var excelReader = new ExcelReader();
            var excelRange = excelReader.ReadExcelData();

            //Data Validation
            var userList = _dataValidation.ValidateExcelDataAndCreateModel(excelRange);
            
            foreach (var user in userList)
            {
                if (user.IsValid)
                {
                    //Premium Calculation for Each Customer
                    _premiumCalculatorInstance.CalculatePremium(user);

                    //Document Generation for Each Customer
                    _documentGeneratorInstance.GenerateRenewalLetter(user);
                }
                else
                {
                    stringBuilder.Append(user.ValidationMessage);
                }
            }

            if (stringBuilder.ToString().Equals(string.Empty))
            {
                result.IsGenerated = true;
                result.Message = "Document Generated";
            }
            else
            {
                result.IsGenerated = false;
                result.Message = stringBuilder.ToString();
            }

            return result;
        }
        
        
        #endregion
    }
}